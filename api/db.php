<?php
define("DB_HOST", "localhost");
define("DB_USER", "username");
define("DB_PASSWORD", "password");
define("DB_NAME", "notifix");

class DbConnect
{
    private $connect;

    public function __construct()
    {
        $this->connect = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

        if (!$this->connect) {
            die('Connect Error: ' . mysqli_connect_errno());
        }
    }

    public function getDb()
    {
        return $this->connect;
    }
}