<?php
define('API', 'https://api.thetvdb.com');
define('API_KEY', '10000000000');
define('API_USERNAME', 'username');
define('API_USERKEY', 'userkey');

//
class ApiConnect
{
    private $token;

    public function __construct()
    {
        if (apc_exists('apiToken')) {
            $this->$token = apc_fetch('apiToken');
        } else {
            $this->$token = $this->getToken();
        }
    }

    public function query($keyword)
    {
        $query = http_build_query([
            'name' => $keyword,
        ]);

        $url = API . '/search/series?' . $query;

        $c = curl_init($url);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer '.$this->$token;

        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true); 

        $result = json_decode(curl_exec($c));

        curl_close($c); 

        return $result;
    }

    public function single($id) {
        $data = [];

        $url = API . "/series/$id";

        $c = curl_init($url);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer '.$this->$token;

        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true); 

        $single = json_decode(curl_exec($c));

        if(!$single->data) {
            die(json_encode(['Error' => 'An error occured when getting this show. It might be banned from our API.']));
        }

        $single = $single->data;        
        $data['id'] = $single->id;
        $data['seriesName'] = $single->seriesName;
        $data['banner'] = $single->banner;
        $data['seriesId'] = $single->seriesId;
        $data['status'] = $single->status;
        $data['network'] = $single->network;
        $data['runtime'] = $single->runtime;
        foreach($single->genre as $genre) {
            $data['genre'][]  = $genre;
        }
        $data['overview'] = $single->overview;
        $data['airsDayOfWeek'] = $single->airsDayOfWeek;
        $data['airsTime'] = $single->airsTime;
        $data['rating'] = $single->rating;
        $data['siteRating'] = $single->siteRating;
        $data['siteRatingCount'] = $single->siteRatingCount;

        curl_close($c);

        $page = 1;
        $last = 0;

        do {
            $url = API . "/series/$id/episodes?page=$page";
            $c = curl_init($url);

            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: Bearer '.$this->$token;

            curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true); 

            $episodes = json_decode(curl_exec($c));
            
            if(!$episodes->data) {
                die(json_encode(['Error' => 'An error occured when getting this show. It might be banned from our API.']));
            }

            $last = $episodes->links->next;

            foreach($episodes->data as $episode) {
                if($episode->airedSeason == 0) {
                    continue;
                }

                $data['data'][] = [
                    "absoluteNumber" => $episode->absoluteNumber,
                    "airedEpisodeNumber" => $episode->airedEpisodeNumber,
                    "airedSeason" => $episode->airedSeason,
                    "airedSeasonID" => $episode->airedSeasonID,
                    "dvdEpisodeNumber" => $episode->dvdEpisodeNumber,
                    "dvdSeason" => $episode->dvdSeason,
                    "episodeName" => $episode->episodeName,
                    "firstAired" => $episode->firstAired,
                    "id" => $episode->id,
                    "language" => $episode->language->overview,
                    "overview" => $episode->overview
                ];
            }

            $page = $page + 1;
            
            curl_close($c);
        } while($last != "null" && $last != null);

        usort($data['data'], function($a, $b) {
            if ($a['airedSeason'] == $b['airedSeason']) return $a['airedEpisodeNumber'] <=> $b['airedEpisodeNumber'];
            return $a['airedSeason'] <=> $b['airedSeason'];
        });


        return $data;
    }

    public function next($id) {
        $data = [];

        $url = API . "/series/$id";

        $c = curl_init($url);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer '.$this->$token;

        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true); 

        $single = json_decode(curl_exec($c));

        if(!$single->data) {
            die(json_encode(['Error' => 'An error occured when getting this show. It might be banned from our API.']));
        }

        $single = $single->data;        
        $data['status'] = $single->status;
        $data['airsTime'] = $single->airsTime;

        curl_close($c);
 
        
        $page = 1;
        $last = 0;

        $allEpisodes = [];

        do {
            $url = API . "/series/$id/episodes?page=$page";
            $c = curl_init($url);

            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: Bearer '.$this->$token;

            curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true); 

            $episodes = json_decode(curl_exec($c));
            
            if(!$episodes->data) {
                die(json_encode(['Error' => 'An error occured when getting this show. It might be banned from our API.']));
            }

            $last = $episodes->links->next;

            foreach($episodes->data as $episode) {
                if($episode->airedSeason == 0) {
                    continue;
                }

                $allEpisodes['data'][] = [
                    "absoluteNumber" => $episode->absoluteNumber,
                    "airedEpisodeNumber" => $episode->airedEpisodeNumber,
                    "airedSeason" => $episode->airedSeason,
                    "airedSeasonID" => $episode->airedSeasonID,
                    "dvdEpisodeNumber" => $episode->dvdEpisodeNumber,
                    "dvdSeason" => $episode->dvdSeason,
                    "episodeName" => $episode->episodeName,
                    "firstAired" => $episode->firstAired,
                    "id" => $episode->id,
                    "language" => $episode->language->overview,
                    "overview" => $episode->overview
                ];
            }

            $page = $page + 1;
            
            curl_close($c);
        } while($last != "null" && $last != null);

        usort($allEpisodes['data'], function($a, $b) {
            if ($a['airedSeason'] == $b['airedSeason']) return $a['airedEpisodeNumber'] <=> $b['airedEpisodeNumber'];
            return $a['airedSeason'] <=> $b['airedSeason'];
        });
        $lastofAll = end($allEpisodes['data']);
        $data['firstAired'] = $lastofAll['firstAired'];

        




        return $data;
    }

    private function getToken() {
        $c = curl_init(API . '/login');

        $data = json_encode([
            'apikey' => API_KEY,
            'username' => API_USERNAME,
            'userkey' => API_USERKEY,
        ]);

        curl_setopt($c, CURLOPT_POSTFIELDS, $data);
        curl_setopt($c, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($c));

        curl_close($c);
        
        if (array_key_exists('Error', $result)) {
            die('An error occured');
        } else {
            apc_store('apiToken', $result->token, 23*60*60);
        }

        return $result;
    }
}