-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 25, 2017 at 07:06 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1-log
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notifix`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Reinier', '$2y$10$7GUsSZ2QG13Dp9I20lyhO.kzZz7GorWcKty3q3E7GtrQqziEaxFyG', 'test@test.com', '2017-01-17 19:49:16', '2017-01-17 19:49:16'),
(9, 'Reinier2', '$2y$10$MEm9if83s3CYZBifh6ue7ORHhB8xK3kjj.gXfS3rXhx7OmzItvwvu', 'test2@test.com', '2017-01-17 20:06:23', '2017-01-17 20:06:23'),
(10, 'Test', '$2y$10$8nu6Zk8gFKDo37R4mPVcP.l1q3h5OKKLdhYDQkRtjyO.RcgavnTI2', 'test@test3.com', '2017-01-17 20:32:54', '2017-01-17 20:32:54'),
(11, 'Test2', '$2y$10$sVDDB8vgRiJ7uXxBQXFO3urwkRCxGE5bkgglEGqBoM5er.VX.6F.q', 'test@test43.com', '2017-01-17 20:38:26', '2017-01-17 20:38:26'),
(12, 'luc', '$2y$10$YLJAsMItWbjd420jdflzquh5DSqn9/dv.yBBzVcKGGZ1n5lFdF1ba', 'lucrunge2@gmail.com', '2017-01-19 09:47:11', '2017-01-19 09:47:11'),
(13, 'lucr', '$2y$10$Jx2bLgMzQOsCTX8Rn1lPmOcemdegh3lJWdk/3JkVV5e4dJW0p6rIC', 'lucrunge3@gmail.com', '2017-01-21 19:30:42', '2017-01-21 19:30:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
