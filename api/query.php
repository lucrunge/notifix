<?php
require 'api.php';

header('Content-Type: application/json');

if(isset($_GET['name']) && !empty($_GET['name'])) {
    $api = new ApiConnect();
    echo json_encode($api->query($_GET['name']));
} else {
    echo json_encode(['Error' => 'Please enter a value']);
}


