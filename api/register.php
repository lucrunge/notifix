<?php
require 'user.php';

header('Content-Type: application/json');

if(isset($_POST['username'], $_POST['password'], $_POST['email']) && !empty($_POST['username']) &&
        !empty($_POST['password']) && !empty($_POST['email'])) {
    $userObject = new User();
    $response = $userObject->createNewRegisterUser($_POST['username'], $_POST['password'], $_POST['email']);
    echo json_encode($response);
}

