<?php
require 'user.php';

header('Content-Type: application/json');

if(isset($_POST['username'], $_POST['password']) && !empty($_POST['username'] && !empty($_POST['password']))) {
    $userObject = new User();
    $response = $userObject->loginUsers($_POST['username'], $_POST['password']);
    echo json_encode($response);
}

