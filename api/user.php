<?php
include_once 'db.php';

class User
{
    private $db;
    private $db_table = "user";

    public function __construct()
    {
        $this->db = new DbConnect();
    }

    public function isLoginExist($username, $password)
    {
        $query = 'SELECT `password` FROM ' . $this->db_table . ' WHERE `username`=? LIMIT 1';
        $result = $this->db->getDb()->prepare($query);
        $result->bind_param("s", $username);
        $result->execute();
        $result->store_result();
        $count = $result->num_rows;

        if ($count > 0) {
            $result->bind_result($hash);
            $result->fetch();

            if(password_verify($password, $hash)) {
                return true;
            }
        }

        return false;
    }

    public function isEmailUsernameExist($username, $email)
    {
        $query = 'SELECT * FROM ' . $this->db_table . ' WHERE `username`=? OR `email`=? LIMIT 1';
        $result = $this->db->getDb()->prepare($query);
        $result->bind_param("ss", $username, $email);

        $result->execute();
        $result->store_result();
        $count = $result->num_rows;

        if ($count > 0) {
            return true;
        }

        return false;
    }

    public function isValidEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    public function createNewRegisterUser($username, $password, $email)
    {
        $isExisting = $this->isEmailUsernameExist($username, $email);

        if ($isExisting) {
            $json['success'] = false;
            $json['message'] = "Error registering. Username or email already exists";
        } else {
            $isValid = $this->isValidEmail($email);
            $hash = password_hash($password, PASSWORD_DEFAULT);

            if ($isValid) {
                $query = "INSERT INTO " . $this->db_table . " (`username`, `password`, `email`, `created_at`, `updated_at`) values (?, ?, ?, NOW(), NOW())";
                $result = $this->db->getDb()->prepare($query);
                $result->bind_param("sss", $username, $hash, $email);

                if ($result->execute()) {
                    $json['success'] = true;
                    $json['message'] = "Successfully registered the user";
                } else {
                    $json['success'] = false;
                    $json['message'] = "Error registering. Username or email already exists";
                }

                $this->db->getDb()->close();
            } else {
                $json['success'] = false;
                $json['message'] = "Error registering. Email is not valid";
            }
        }

        return $json;
    }

    public function loginUsers($username, $password)
    {
        $json = array();
        $canUserLogin = $this->isLoginExist($username, $password);

        if ($canUserLogin) {
            $json['success'] = true;
            $json['message'] = "Successfully logged in";
        } else {
            $json['success'] = false;
            $json['message'] = "Incorrect details";
        }
        return $json;
    }
}