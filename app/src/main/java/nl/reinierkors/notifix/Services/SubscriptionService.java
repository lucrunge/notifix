package nl.reinierkors.notifix.Services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.ProgressBar;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import nl.qbusict.cupboard.QueryResultIterable;
import nl.reinierkors.notifix.Database.DatabaseHelper;
import nl.reinierkors.notifix.Models.Serie;
import nl.reinierkors.notifix.Models.Subscription;
import nl.reinierkors.notifix.R;
import nl.reinierkors.notifix.SerieClient;
import nl.reinierkors.notifix.Utility.UtilityFunctions;

import static java.util.concurrent.TimeUnit.*;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by Reinier on 025 25-1-2017 jan.
 */

public class SubscriptionService extends IntentService {
    private SerieClient client;
    String status;
    String airsTime;
    String firstAired;

    public SubscriptionService() {
        super("SubscriptionService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        UtilityFunctions uf = new UtilityFunctions(this);

        Log.i("SubscriptionService", "Service running");

        DatabaseHelper dbHelper = new DatabaseHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor subscriptions = cupboard().withDatabase(db).query(Subscription.class).getCursor();

        createNotification(57, R.mipmap.ic_launcher, "Game of Thrones Alert",
                "Game of Thrones starts at 9:00 PM on HBO");

        try {
            QueryResultIterable<Subscription> itr = cupboard().withCursor(subscriptions).iterate(Subscription.class);

            for (Subscription subscription : itr) {
                if (Objects.equals(subscription.status, "Continuing")) {
                    getLastEpisode(subscription); // kijk of er een serie begint

                    if (uf.isOnline()) {
                        cupboard().withDatabase(db).put(updateSubscription(subscription)); // update de database met nieuwe tijden
                    }
                }
            }
        } finally {
            subscriptions.close();
        }

        Log.i("SubscriptionService", "Service completed.");
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void getLastEpisode(Subscription subscription) {
        String subDate = subscription.firstAired + " " + subscription.airsTime;

        DateTime date = DateTime.parse(subDate,
                DateTimeFormat.forPattern("yyyy-mm-dd hh:mm a"));

        long MIN_DURATION = MILLISECONDS.convert(4, HOURS);
        long MAX_DURATION = MILLISECONDS.convert(5, HOURS);

        // Als vandaag de nieuwe aflevering afspeelt
        if ((date.toLocalDate()).equals(new LocalDate())) {
            long duration = new DateTime().getMillis() - date.getMillis();

            if (duration >= MIN_DURATION && duration <= MAX_DURATION) {
                createNotification(57, R.mipmap.ic_launcher, subscription.name + " Alert",
                        subscription.name + " starts at " + subscription.airsTime + " on " + subscription.network);
            }
        }
    }

    private void createNotification(int nId, int iconRes, String title, String body) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.ic_stat_notifications_active)
                .setContentTitle(title)
                .setContentText(body);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(nId, mBuilder.build());
    }

    private Subscription updateSubscription(Subscription subscription) {
        client = new SerieClient();
        client.getNextEpisode(String.valueOf(subscription.id), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (response != null) {
                        status = response.getString("status");
                        airsTime = response.getString("airsTime");
                        firstAired = response.getString("firstAired");
                    }
                } catch (JSONException e) {
                    //
                }
            }
        });

        if (status != null && airsTime != null && firstAired != null) {
            subscription.status = status;
            subscription.airsTime = airsTime;
            subscription.firstAired = firstAired;
        }

        return subscription;
    }
}
