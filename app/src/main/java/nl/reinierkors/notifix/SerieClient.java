package nl.reinierkors.notifix;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Reinier on 021 21-1-2017 jan.
 */

public class SerieClient {
    private static final String API_BASE_URL = "http://dev.idres.nl/";
    private AsyncHttpClient client;

    public SerieClient() {
        this.client = new AsyncHttpClient();
    }

    private String getApiUrl(String relativeUrl) {
        return API_BASE_URL + relativeUrl;
    }

    public void getSeries(final String query, JsonHttpResponseHandler handler) {
        try {
            String url = getApiUrl("query.php?name=");
            client.get(url + URLEncoder.encode(query, "utf-8"), handler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void getExtraSerieDetails(String id, JsonHttpResponseHandler handler) {
        String url = getApiUrl("single.php?name=");
        try {
            client.get(url + URLEncoder.encode(id, "utf-8"), handler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void getNextEpisode(String id, JsonHttpResponseHandler handler) {
        String url = getApiUrl("next.php?name=");
        try {
            client.get(url + URLEncoder.encode(id, "utf-8"), handler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
