package nl.reinierkors.notifix.Receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import nl.reinierkors.notifix.Services.SubscriptionService;

/**
 * Created by Reinier on 025 25-1-2017 jan.
 */

public class BootBroadcastReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent startServiceIntent = new Intent(context, SubscriptionService.class);
        startWakefulService(context, startServiceIntent);
    }
}
