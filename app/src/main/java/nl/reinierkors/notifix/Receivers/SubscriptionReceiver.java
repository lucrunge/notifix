package nl.reinierkors.notifix.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import nl.reinierkors.notifix.Services.SubscriptionService;

/**
 * Created by Reinier on 025 25-1-2017 jan.
 */

public class SubscriptionReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;

    // Triggered by the Alarm periodically (starts the service to run task)
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, SubscriptionService.class);
        context.startService(i);
    }
}
