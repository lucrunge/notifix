package nl.reinierkors.notifix.Models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import nl.reinierkors.notifix.Database.DatabaseHelper;
import nl.reinierkors.notifix.SerieClient;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by Reinier on 021 21-1-2017 jan.
 */

public class Serie implements Serializable {
    private String aliases;
    private String banner;
    private String firstAired;
    private int id;
    private String network;
    private String overview;
    private String seriesName;
    private String status;
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;
    private SerieClient client;
    private String airsTime;

    public Serie() {

    }

    public Serie(int id, String seriesName, String banner, String overview, String network, String status, String airsTime) {
        this.id = id;
        this.seriesName = seriesName;
        this.banner = banner;
        this.overview = overview;
        this.network = network;
        this.status = status;
        this.airsTime = airsTime;
    }

    public String getAliases() {
        return aliases;
    }

    public String getFirstAired() {
        return firstAired;
    }

    public int getId() {
        return id;
    }

    public String getNetwork() {
        return network;
    }

    public String getOverview() {
        return overview;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public String getStatus() {
        return status;
    }

    public String getCoverUrl() {
        return "https://thetvdb.com/banners/" + banner;
    }

    public String getBannerUrl() {
        return "https://thetvdb.com/banners/posters/" + id + "-1.jpg";
    }

    public static Serie fromJson(JSONObject jsonObject) {
        Serie serie = new Serie();
        try {
            serie.aliases = getAliases(jsonObject);
            serie.banner = jsonObject.getString("banner");
            serie.firstAired = jsonObject.getString("firstAired");
            serie.id = jsonObject.getInt("id");
            serie.network = jsonObject.getString("network");

            if(!Objects.equals(jsonObject.getString("overview"), "null")) {
                serie.overview = jsonObject.getString("overview");
            } else {
                serie.overview = "No description";
            }

            serie.seriesName = jsonObject.getString("seriesName");
            serie.status = jsonObject.getString("status");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return serie;
    }

    // Return comma separated author list when there is more than one author
    private static String getAliases(final JSONObject jsonObject) {
        try {
            final JSONArray authors = jsonObject.getJSONArray("aliases");
            int numAuthors = authors.length();
            final String[] authorStrings = new String[numAuthors];
            for (int i = 0; i < numAuthors; ++i) {
                authorStrings[i] = authors.getString(i);
            }
            return TextUtils.join(", ", authorStrings);
        } catch (JSONException e) {
            return "";
        }
    }

    public static ArrayList<Serie> fromJson(JSONArray jsonArray) {
        ArrayList<Serie> series = new ArrayList<Serie>(jsonArray.length());

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject serieJson = null;
            try {
                serieJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Serie serie = Serie.fromJson(serieJson);
            if (serie != null) {
                series.add(serie);
            }
        }
        return series;
    }
}
