package nl.reinierkors.notifix.Models;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Reinier on 022 22-1-2017 jan.
 */

public class Episode implements Serializable {
    public int airedSeason;
    public int airedEpisodeNumber;
    public String episodeName;
    public String firstAired;
    public String status;
    public String overview;

    public Episode(int airedSeason, int airedEpisodeNumber, String episodeName, String firstAired, String overview) {
        this.airedSeason = airedSeason;
        this.airedEpisodeNumber = airedEpisodeNumber;
        if (episodeName == null || Objects.equals(episodeName, "null")) {
            this.episodeName = "Unknown";
        } else {
            this.episodeName = episodeName;
        }
        this.firstAired = firstAired;
        if (overview == null || Objects.equals(overview, "null")) {
            this.overview = "Unknown";
        } else {
            this.overview = overview;
        }
        this.status = getStatus(overview);
    }

    private String getStatus(String overview) {
        String theStatus;

        if(overview == null || Objects.equals(overview, "null")) {
            theStatus = "Unaired";
        } else {
            theStatus = "Aired";
        }

        return theStatus;
    }
}
