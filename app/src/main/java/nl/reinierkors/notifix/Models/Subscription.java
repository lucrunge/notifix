package nl.reinierkors.notifix.Models;

/**
 * Created by Reinier on 025 25-1-2017 jan.
 */

public class Subscription {
    public Long _id; // for cupboard
    public String name;
    public String network;
    public String status;
    public String firstAired;
    public String airsTime;
    public int id;

    public Subscription() {

    }

    public Subscription(String nextDate, String time, String name, String network, String status, int id) {
        this.firstAired = nextDate;
        this.airsTime = time;
        this.name = name;
        this.network = network;
        this.status = status;
        this.id = id;
    }
}
