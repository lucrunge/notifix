package nl.reinierkors.notifix;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.reinierkors.notifix.Adapters.EpisodeAdapter;
import nl.reinierkors.notifix.Models.Episode;

public class EpisodesActivity extends AppCompatActivity {
    @BindView(R.id.lvItems) ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episodes);
        ButterKnife.bind(this);
        Intent intent = getIntent();

        @SuppressWarnings("unchecked")
        ArrayList<Episode> arrayOfEpisodes = (ArrayList<Episode>) intent.getSerializableExtra("episodes");
        String title = intent.getStringExtra("title");
        this.setTitle(title);
        EpisodeAdapter adapter = new EpisodeAdapter(this, arrayOfEpisodes);
        listView.setAdapter(adapter);

        Snackbar.make(findViewById(android.R.id.content), arrayOfEpisodes.size() +  " episodes found!", Snackbar.LENGTH_SHORT)
                .show();
    }
}
