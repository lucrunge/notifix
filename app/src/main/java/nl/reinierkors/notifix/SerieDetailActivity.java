package nl.reinierkors.notifix;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import nl.reinierkors.notifix.Adapters.EpisodeAdapter;
import nl.reinierkors.notifix.Database.DatabaseHelper;
import nl.reinierkors.notifix.Models.Episode;
import nl.reinierkors.notifix.Models.Serie;
import nl.reinierkors.notifix.Models.Subscription;
import nl.reinierkors.notifix.Utility.UtilityFunctions;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class SerieDetailActivity extends AppCompatActivity {
    private SerieClient client;
    @BindView(R.id.ivCover) ImageView ivCover;
    @BindView(R.id.tvOverview) TextView tvOverview;
    @BindView(R.id.tvGenres) TextView tvGenres;
    @BindView(R.id.tvStatus) TextView tvStatus;
    @BindView(R.id.tvNetwork) TextView tvNetwork;
    @BindView(R.id.tvAirTime) TextView tvAirtime;
    @BindView(R.id.tvRating) TextView tvRating;
    @BindView(R.id.allEpisodes) Button allEpisodes;
    @BindView(R.id.subscribe) Button subscribeButton;
    private ArrayList<Episode> arrayOfEpisodes;
    private Serie serie;
    private ProgressBar progress;
    private UtilityFunctions uf;
    private String airsTime;
    private SQLiteDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serie_detail);

        ButterKnife.bind(this);

        DatabaseHelper dbHelper = new DatabaseHelper(this);
        db = dbHelper.getWritableDatabase();

        arrayOfEpisodes = new ArrayList<>();

        uf = new UtilityFunctions(this);
        serie = (Serie) getIntent().getSerializableExtra("serie");
        if(isSubscribed()) {
            subscribeButton.setText("Unsubscribe");
        }
        progress = (ProgressBar) findViewById(R.id.progress);
        loadSerie(serie);
    }

    @OnClick(R.id.subscribe)
    public void subscribeClick() {
        if(!uf.isOnline()) {
            Snackbar.make(findViewById(android.R.id.content), "Could not connect to the internet.", Snackbar.LENGTH_LONG)
                    .show();
            return;
        }

        if(!isSubscribed()) {
            // subscription does not yet exist, create it
            subscribe();
            subscribeButton.setText("Unsubscribe");

            Snackbar.make(findViewById(android.R.id.content), "Succesfully subcribed to " + serie.getSeriesName() +"!", Snackbar.LENGTH_LONG)
                    .show();
        } else {
            // unsubcribe
            unsubscribe();
            subscribeButton.setText("Get notifications");

            Snackbar.make(findViewById(android.R.id.content), "Unsubcribed to " + serie.getSeriesName() +"!", Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    public boolean isSubscribed() {
        if(cupboard().withDatabase(db).query(Subscription.class).withSelection("id = ?", String.valueOf(serie.getId())).get() == null) {
            return false;
        }
        return true;
    }

    private void createNotification(int nId, int iconRes, String title, String body) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.ic_stat_notifications_active)
                .setContentTitle(title)
                .setContentText(body);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(nId, mBuilder.build());
    }

    public void unsubscribe() {
        cupboard().withDatabase(db).delete(Subscription.class, "id = ?", String.valueOf(serie.getId()));
    }

    public void subscribe() {
        client = new SerieClient();
        client.getNextEpisode(String.valueOf(serie.getId()), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if (response != null) {
                        cupboard().withDatabase(db).put(new Subscription(serie.getFirstAired(), response.getString("airsTime"), serie.getSeriesName(), serie.getNetwork(), serie.getStatus(), serie.getId()));

                        createNotification(57, R.drawable.ic_launcher, serie.getSeriesName() + " Alert",
                                serie.getSeriesName() + " starts at " + response.getString("airsTime") + " on " + serie.getNetwork());
                    }
                } catch (JSONException e) {
                    //
                }
            }
        });
    }


    @OnClick(R.id.allEpisodes)
    public void allEpisodes() {
        Intent intent = new Intent(SerieDetailActivity.this, EpisodesActivity.class);
        intent.putExtra("episodes", arrayOfEpisodes);
        intent.putExtra("title", serie.getSeriesName());
        startActivity(intent);
    }

    private void loadSerie(Serie serie) {
        progress.setVisibility(ProgressBar.VISIBLE);
        this.setTitle(serie.getSeriesName());
        Picasso.with(this).load(Uri.parse(serie.getCoverUrl())).error(R.drawable.ic_noheader).into(ivCover);
        tvOverview.setText(serie.getOverview());
        tvStatus.setText("Status: " + serie.getStatus());
        tvNetwork.setText("Network: " + serie.getNetwork());
        SerieClient client = new SerieClient();
        client.getExtraSerieDetails(serie.getId() + "", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    progress.setVisibility(ProgressBar.GONE);
                    allEpisodes.setVisibility(Button.VISIBLE);
                    //if (response.has("shows")) {

                    tvAirtime.setText("AirTime: " + response.getString("airsDayOfWeek") + " " + response.getString("airsTime"));
                    tvRating.setText("Rating: " + response.getInt("siteRating") + "/10");
                    final JSONArray genres = response.getJSONArray("genre");
                    final int numGenres = genres.length();
                    final String[] allGenres = new String[numGenres];
                    for (int i = 0; i < numGenres; ++i) {
                        allGenres[i] = genres.getString(i);
                    }
                    tvGenres.setText("Genres: " + TextUtils.join(", ", allGenres));

                    if (response.has("data")) {
                        final JSONArray data = response.getJSONArray("data");
                        for (int i = 0; i < data.length(); ++i) {
                            JSONObject epi = data.getJSONObject(i);
                            int season = epi.getInt("airedSeason");
                            int episode = epi.getInt("airedEpisodeNumber");
                            String name = epi.getString("episodeName");
                            String aired = epi.getString("firstAired");
                            String overview = epi.getString("overview");

                            Episode epis = new Episode(season, episode, name, aired, overview);
                            arrayOfEpisodes.add(epis);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.setVisibility(ProgressBar.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_share) {
            setShareIntent();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setShareIntent() {
        Uri bmpUri = getLocalBitmapUri(ivCover);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("*/*");
        shareIntent.putExtra(Intent.EXTRA_TEXT, serie.getSeriesName());
        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
        startActivity(Intent.createChooser(shareIntent, "Share Image"));
    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }

        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}
