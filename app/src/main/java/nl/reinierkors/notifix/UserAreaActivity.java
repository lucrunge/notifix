package nl.reinierkors.notifix;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import nl.qbusict.cupboard.QueryResultIterable;
import nl.reinierkors.notifix.Adapters.SerieAdapter;
import nl.reinierkors.notifix.Adapters.SubscriptionAdapter;
import nl.reinierkors.notifix.Database.DatabaseHelper;
import nl.reinierkors.notifix.Models.Episode;
import nl.reinierkors.notifix.Models.Serie;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import nl.reinierkors.notifix.Models.Subscription;
import nl.reinierkors.notifix.Receivers.SubscriptionReceiver;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class UserAreaActivity extends AppCompatActivity {
    private ListView lvSeries;
    private SubscriptionAdapter subscriptionAdapter;
    private ProgressBar progress;
    private SearchView searchView;
    private MenuItem searchItem;
    private Menu mOptionsMenu;

    @Override
    public void onResume()
    {
        super.onResume();
        fetchSeries();
        if (mOptionsMenu != null) {
            searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            searchItem = mOptionsMenu.findItem(R.id.action_search);
            searchView.clearFocus();
            searchView.setQuery("", false);
            searchView.setIconified(true);
            searchItem.collapseActionView();
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();

        View empty = findViewById(R.id.empty);
        ListView list = (ListView) findViewById(R.id.lvSeries);
        list.setEmptyView(empty);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_area);
        ButterKnife.bind(this);
        Snackbar.make(findViewById(android.R.id.content), "Logged in!", Snackbar.LENGTH_SHORT)
                .show();
        lvSeries = (ListView) findViewById(R.id.lvSeries);
        ArrayList<Subscription> aSubscriptions = new ArrayList<>();
        subscriptionAdapter = new SubscriptionAdapter(this, aSubscriptions);
        lvSeries.setAdapter(subscriptionAdapter);
        progress = (ProgressBar) findViewById(R.id.progress);

        fetchSeries();
        setupSerieSelectedListener();
        scheduleSubscriptions();
    }

    public void setupSerieSelectedListener() {
        lvSeries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                progress.setVisibility(ProgressBar.VISIBLE);
                SerieClient client = new SerieClient();
                final Subscription subscription = subscriptionAdapter.getItem(position);
                client.getExtraSerieDetails( subscription.id + "", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            Serie serie = new Serie(response.getInt("id"), response.getString("seriesName"),
                                    response.getString("banner"), response.getString("overview"),
                                    response.getString("network"), response.getString("status"),
                                    response.getString("airsTime"));

                                    Intent intent = new Intent(UserAreaActivity.this, SerieDetailActivity.class);
                                    intent.putExtra("serie", serie);
                                    startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        progress.setVisibility(ProgressBar.GONE);
                    }
                });
            }
        });
    }

    private void fetchSeries() {
        progress.setVisibility(ProgressBar.VISIBLE);

        subscriptionAdapter.clear();

        DatabaseHelper dbHelper = new DatabaseHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor subscriptions = cupboard().withDatabase(db).query(Subscription.class).getCursor();
        try {
            // Iterate Bunnys
            QueryResultIterable<Subscription> itr = cupboard().withCursor(subscriptions).iterate(Subscription.class);
            for (Subscription subscription : itr) {
                subscriptionAdapter.add(subscription);
            }
        } finally {
            // close the cursor
            subscriptions.close();
            progress.setVisibility(ProgressBar.GONE);
        }

        subscriptionAdapter.notifyDataSetChanged();
    }

    public void scheduleSubscriptions() {
        // Construct an intent that will execute the AlarmReceiver
        Intent intent = new Intent(getApplicationContext(), SubscriptionReceiver.class);
        // Create a PendingIntent to be triggered when the alarm goes off
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, SubscriptionReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Setup periodic alarm every 5 seconds
        long firstMillis = System.currentTimeMillis(); // alarm is set right away
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        // First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
        // Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis,
                AlarmManager.INTERVAL_HOUR, pIntent);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mOptionsMenu = menu;
        getMenuInflater().inflate(R.menu.serie_list, menu);
        searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // new intent
                Intent intent = new Intent(UserAreaActivity.this, SearchActivity.class);
                intent.putExtra("query", query);
                UserAreaActivity.this.startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }
}
