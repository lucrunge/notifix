package nl.reinierkors.notifix.Adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import nl.reinierkors.notifix.Models.Serie;
import nl.reinierkors.notifix.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Reinier on 021 21-1-2017 jan.
 */

public class SerieAdapter extends ArrayAdapter<Serie> {
    private static class ViewHolder {
        public ImageView ivCover;
        public TextView tvTitle;
        public TextView tvOverview;
    }

    public SerieAdapter(Context context, ArrayList<Serie> aSerie) {
        super(context, 0, aSerie);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
// Get the data item for this position
        final Serie serie = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.items, parent, false);
            viewHolder.ivCover = (ImageView)convertView.findViewById(R.id.ivCover);
            viewHolder.tvTitle = (TextView)convertView.findViewById(R.id.tvTitle);
            viewHolder.tvOverview = (TextView)convertView.findViewById(R.id.tvOverview);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data into the template view using the data object
        viewHolder.tvTitle.setText(serie.getSeriesName());
        viewHolder.tvOverview.setText(serie.getOverview());
        Picasso.with(getContext()).load(Uri.parse(serie.getBannerUrl())).error(R.drawable.ic_nocover).into(viewHolder.ivCover);
        // Return the completed view to render on screen
        return convertView;
    }
}
