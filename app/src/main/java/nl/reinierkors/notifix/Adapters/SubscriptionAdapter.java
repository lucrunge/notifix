package nl.reinierkors.notifix.Adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import nl.reinierkors.notifix.Models.Subscription;
import nl.reinierkors.notifix.R;

/**
 * Created by Reinier on 025 25-1-2017 jan.
 */

public class SubscriptionAdapter extends ArrayAdapter<Subscription> {
    private static class ViewHolder {
        public TextView tvName;
        public TextView tvDescription;
        public TextView tvStatus;
    }

    public SubscriptionAdapter(Context context, ArrayList<Subscription> aSubscription) {
        super(context, 0, aSubscription);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    // Get the data item for this position
        final Subscription subscription = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        SubscriptionAdapter.ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new SubscriptionAdapter.ViewHolder();
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.subscriptions, parent, false);
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.tvDescription = (TextView)convertView.findViewById(R.id.tvDescription);
            viewHolder.tvStatus = (TextView)convertView.findViewById(R.id.tvStatus);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SubscriptionAdapter.ViewHolder) convertView.getTag();
        }
        // Populate the data into the template view using the data object
        viewHolder.tvName.setText(subscription.name);
        viewHolder.tvDescription.setText("Airs on " + subscription.firstAired + " at " + subscription.airsTime);
        viewHolder.tvStatus.setText(subscription.status);

        // Return the completed view to render on screen
        return convertView;
    }
}
