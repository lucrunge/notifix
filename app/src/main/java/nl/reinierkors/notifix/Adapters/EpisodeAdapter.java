package nl.reinierkors.notifix.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import nl.reinierkors.notifix.Models.Episode;
import nl.reinierkors.notifix.R;

/**
 * Created by Reinier on 022 22-1-2017 jan.
 */

public class EpisodeAdapter extends ArrayAdapter<Episode> {
    public EpisodeAdapter(Context context, ArrayList<Episode> episodes) {
        super(context, 0, episodes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Episode episode = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.episodes, parent, false);
        }

        TextView tvSeason = (TextView) convertView.findViewById(R.id.tvSeason);
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
        TextView tvOverview = (TextView) convertView.findViewById(R.id.tvOverview);

        tvSeason.setText("Season " + String.valueOf(episode.airedSeason) + " - Eps. " + String.valueOf(episode.airedEpisodeNumber));
        tvName.setText(String.valueOf(episode.episodeName));
        tvStatus.setText(String.valueOf(episode.status) + " " + String.valueOf(episode.firstAired));
        tvOverview.setText(String.valueOf(episode.overview));
        return convertView;
    }
}
