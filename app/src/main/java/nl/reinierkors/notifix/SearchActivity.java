package nl.reinierkors.notifix;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import nl.reinierkors.notifix.Adapters.SerieAdapter;
import nl.reinierkors.notifix.Models.Serie;

public class SearchActivity extends AppCompatActivity {
    private ListView lvSeries;
    private SerieAdapter serieAdapter;
    private SerieClient client;
    private ProgressBar progress;
    private SearchView searchView;
    private MenuItem searchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        lvSeries = (ListView) findViewById(R.id.lvSeries);
        ArrayList<Serie> aSeries = new ArrayList<Serie>();
        serieAdapter = new SerieAdapter(this, aSeries);
        lvSeries.setAdapter(serieAdapter);
        progress = (ProgressBar) findViewById(R.id.progress);
        setupSerieSelectedListener();

        String query = getIntent().getStringExtra("query");
        fetchSeries(query);
        SearchActivity.this.setTitle(query);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.serie_list, menu);
        searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchSeries(query);
                searchView.clearFocus();
                searchView.setQuery("", false);
                searchView.setIconified(true);
                searchItem.collapseActionView();
                SearchActivity.this.setTitle(query);
                return true;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return true;
    }


    public void setupSerieSelectedListener() {
        lvSeries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Launch the detail view passing book as an extra
                Intent intent = new Intent(SearchActivity.this, SerieDetailActivity.class);
                intent.putExtra("serie", serieAdapter.getItem(position));
                startActivity(intent);
            }
        });
    }

    private void fetchSeries(String query) {
        progress.setVisibility(ProgressBar.VISIBLE);

        client = new SerieClient();
        client.getSeries(query, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    progress.setVisibility(ProgressBar.GONE);
                    JSONArray data = null;
                    if (response != null) {
                        data = response.getJSONArray("data");
                        final ArrayList<Serie> series = Serie.fromJson(data);
                        serieAdapter.clear();
                        for (Serie serie : series) {
                            if(Objects.equals(serie.getSeriesName(), "** 403: Series Not Permitted **")) {
                                continue;
                            }
                            serieAdapter.add(serie);
                        }
                        serieAdapter.notifyDataSetChanged();
                    }
                    Snackbar.make(findViewById(android.R.id.content), serieAdapter.getCount() +  " TV Shows found!", Snackbar.LENGTH_SHORT)
                            .show();
                } catch (JSONException e) {
                    Snackbar.make(findViewById(android.R.id.content), "No shows found!", Snackbar.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.setVisibility(ProgressBar.GONE);
            }
        });
    }
}
